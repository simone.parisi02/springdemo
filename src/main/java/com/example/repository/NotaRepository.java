package com.example.repository;

import com.example.domain.Nota;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface NotaRepository extends JpaRepository<Nota,Long>{
    
}
