package com.example.demo;

import com.example.domain.Attore;
import com.example.domain.Country;
import com.example.domain.Nota;
import com.example.repository.AttoreRepository;
import com.example.repository.NotaRepository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoApplicationTests {

	@Autowired
	AttoreRepository attoreRepository;
	@Autowired
	NotaRepository notaRepository;
	@Test
	void contextLoads() {
		Attore a = new Attore();
		a.setAnnoNascita(1999L);
		a.setNome("test mlml");
		a.setCodAttore(1L);
		Country c = new Country();
		c.setCountry("ita");
		a.setCountry(c);
		Nota n = new Nota();
		n.setNota("sono una nota");
		notaRepository.save(n);
		a.setNota(n);
		attoreRepository.save(a);
	}

}
