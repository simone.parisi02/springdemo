package com.example.service;

import java.util.Optional;

import com.example.domain.Attore;
import com.example.domain.Nota;
import com.example.repository.NotaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotaService {
    @Autowired
    NotaRepository notaRepository;
    @Autowired
    AttoreService attoreService;

    public void save(Nota nota, Long codAttore){
        notaRepository.save(nota);
        Optional<Attore> optAttore= attoreService.findByID(codAttore);
        if(optAttore.isPresent()){
            Attore attore = optAttore.get();
            attore.setNota(nota);
            attoreService.save(attore);
        }
    }
}
