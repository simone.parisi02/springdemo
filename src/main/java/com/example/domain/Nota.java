package com.example.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="note")
@Getter @Setter @ToString
public class Nota {
    @Id
    @Column(name="ID")
    Long id;

    @OneToOne(mappedBy = "nota")
    Attore attore;

    @Lob
    String nota;
}
