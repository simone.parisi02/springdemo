package com.example.components;

import java.util.Objects;

import org.springframework.stereotype.Component;

@Component
public class MiaClasse {
    String titoto = "test";


    public MiaClasse() {
    }

    public MiaClasse(String titoto) {
        this.titoto = titoto;
    }

    public String getTitoto() {
        return this.titoto;
    }

    public void setTitoto(String titoto) {
        this.titoto = titoto;
    }

    public MiaClasse titoto(String titoto) {
        this.titoto = titoto;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof MiaClasse)) {
            return false;
        }
        MiaClasse miaClasse = (MiaClasse) o;
        return Objects.equals(titoto, miaClasse.titoto);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(titoto);
    }

    @Override
    public String toString() {
        return "{" +
            " titoto='" + getTitoto() + "'" +
            "}";
    }

}
