package com.example.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.annotations.Cascade;

import lombok.*;

@Entity
@Table(name="countries")
@Getter @Setter @ToString
public class Country {
    @Id
    @Column(
        name = "country",
        length = 20,
        nullable = false
    )
    String country;
    
    @OneToMany(
        mappedBy = "country",
        cascade = CascadeType.ALL,
        orphanRemoval = true
    )
    @JsonIgnore
    Set<Attore> attore;

}
