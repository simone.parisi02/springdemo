package com.example.domain;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="tabella_tipi_sql_java")
@Getter @Setter @ToString
public class TabellaTipiSQL_JAVA {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long        tipoLong_id; 
    boolean     tipoBoolean;
    byte[]      tipoVettoreByte;
    int         tipoIntero;
    float       tipoFloat;
    double      tipoDouble;

    @Column(precision=18, scale=4)
    BigDecimal  tipoBigDecimal;

    Date        tipoData; // con informazioni di time zone

    @CreationTimestamp
    Timestamp   tipoDateTimestamp_creation;

    @UpdateTimestamp
    Timestamp   tipoDateTimestamp_update;

    Time        tipoTime;
    LocalDate   tipoLocalDate; // senza informazioni di time zone
    LocalTime   tipoLocalTime;
    LocalDateTime tipoLocalDateTime;
    
    @Column(columnDefinition="YEAR")
    Short       tipoYear; 
    
    char        tipoChar;
    @Column(length=20)
    String      tipoVarchar;

    @Column(columnDefinition="TEXT")
    String      tipoText;

    @Lob
    String      tipoLongText;

    @Lob
    byte[]      tipoLongBlob;

}
