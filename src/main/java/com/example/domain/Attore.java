package com.example.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;


import com.fasterxml.jackson.annotation.JsonIdentityReference;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="attori")
//Lombok
@Getter 
@Setter 
@ToString
public class Attore {
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY) // auto-increment
    @Column(name="cod_attore")
    
    private Long codAttore;
    private String nome;

    @Column(name="anno_nascita")
    private Long annoNascita;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "country")
    @JsonIdentityReference
    private Country country;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "nota_id", referencedColumnName = "id")
    private Nota nota;
}
