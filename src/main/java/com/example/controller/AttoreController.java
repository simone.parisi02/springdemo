package com.example.controller;

import java.util.List;
import java.util.Optional;

import com.example.components.MiaClasse;
import com.example.domain.Attore;
import com.example.dto.AttoriPar;
import com.example.service.AttoreService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/attori")
@CrossOrigin(origins = "*", maxAge = 3600)
public class AttoreController {
    
    /* @Autowired */
    AttoreService attoreService;
    MiaClasse miaClasse;

    public AttoreController(AttoreService attoreService, MiaClasse miaClasse){
        this.attoreService = attoreService;
        this.miaClasse = miaClasse;
    }

    @GetMapping("/c")
    public ResponseEntity<?> c(){
        return new ResponseEntity<String>(miaClasse.getTitoto(), HttpStatus.OK);
    }


    @GetMapping()
    public ResponseEntity<?> findAll(
        @RequestParam(required = false,name = "page", defaultValue = "0") int page,
        @RequestParam(required = false,name = "size", defaultValue = "5") int size,
        @RequestParam(required = false,name = "order", defaultValue = "asc") String ascdesc,
        @RequestParam(required = false,name = "field", defaultValue = "codAttore") String field)
    {
        Sort sort;
        if(ascdesc.equals("asc")){
            sort = Sort.by(field);
        } else {
            sort = Sort.by(field).descending();
        }
        PageRequest request = PageRequest.of(page,size,sort);
        return new ResponseEntity<List<Attore>>(attoreService.findAllPage(request),HttpStatus.OK);
    }

    @PostMapping("/dto")
    public void dto(@RequestBody AttoriPar a){
        System.out.print(a.toString());
    }

    @GetMapping("/{codAttore}")
    public ResponseEntity<?> findByID(@PathVariable("codAttore") Long codAttore){
        Optional<Attore> optionalAttore = attoreService.findByID(codAttore);
        if(optionalAttore.isPresent()){
            Attore attore = optionalAttore.get();
            return new ResponseEntity<Attore>(attore, HttpStatus.OK);
        } 
        return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }

    @PostMapping()
    public ResponseEntity<?> save(@RequestBody Attore attore){
        attoreService.save(attore);
        return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{codAttore}")
    public ResponseEntity<?> deleteByID(@PathVariable("codAttore") Long codAttore){
        Optional<Attore> opAttore = attoreService.deleteById(codAttore);
        if(opAttore.isPresent()){
            return new ResponseEntity<Void>(HttpStatus.OK);
        }
        return new ResponseEntity<Void>(HttpStatus.NOT_FOUND); 
    }
    
}
