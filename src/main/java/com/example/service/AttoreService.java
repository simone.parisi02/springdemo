package com.example.service;

import java.util.List;
import java.util.Optional;

import com.example.domain.Attore;
import com.example.repository.AttoreRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class AttoreService {
    @Autowired
    AttoreRepository attoreRepository;

    public List<Attore> findAllPage(PageRequest request) {
        return attoreRepository.findAll(request).getContent();
    }

    public Optional<Attore> findByID(Long codAttore) {
        return attoreRepository.findById(codAttore);
    }

    public void save(Attore attore){
        attoreRepository.save(attore);
    }

    public Optional<Attore> deleteById(Long codAttore){
        Optional<Attore> optAttore = attoreRepository.findById(codAttore);
        if(optAttore.isPresent()){
            attoreRepository.deleteById(codAttore);
            return optAttore;
        }
        return Optional.empty(); 
             
    }

    public Optional<Attore> put(Attore attore){
        Optional<Attore> optAttore = attoreRepository.findById(attore.getCodAttore());
        if(optAttore.isPresent()){
            attoreRepository.save(attore);
            return optAttore;
        } else {
            return Optional.empty();
        }
    }
}
