package com.example.repository;
import com.example.domain.TabellaTipiSQL_JAVA;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TabellaTipiSQL_JAVARepository extends JpaRepository<TabellaTipiSQL_JAVA,Long>{
    
}
